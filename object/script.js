// Object Javascript
// 1. Object Literal
//  Pros: Simple
//  Cons: Not effective for a lot of object

// let character1 = {
//     name:'Basofi',
//     energy:10,
//     eat:function (portion) {
//         this.energy += portion;
//         console.log(`Welcome ${this.name}, bon appetite!`)
//     }
// }
// let character2 = {
//     name:'Sudirman',
//     energy:15,
//     eat:function (portion) {
//         this.energy += portion;
//         console.log(`Welcome ${this.name}, bon appetite!`)
//     }
// }

//2. Function Declaration
//  Pros: effective for a lot of object
//  Cons: uses multiple sources because it always runs all the functions even though it hasn't been called yet

// function Character(name,energy) {
//     let character = {};
//     character.name = name;
//     character.energy = energy;

//     character.eat = function (portion) {
//         this.energy += portion;
//         console.log(`Welcome ${this.name}, bon appetite!`)
//     }

//     character.play = function (hour) {
//         this.energy -= hour
//         console.log(`Welcome ${this.name}, enjoy the game`)
//     }

//     return character;
// }

// let basofi = Character('Basofi',10);
// let sudirman = Character('Sudirman',15);

//3. Constructor Function
//  Pros: needs no variable declarations and return value
//  Cons: uses multiple sources because it always runs all the functions even though it hasn't been called yet

// function Character(name,energy) {
//     this.name = name;
//     this.energy = energy;

//     this.eat = function (portion) {
//         this.energy += portion;
//         console.log(`Welcome ${this.name}, bon appetite!`)
//     }

//     this.play = function (hour) {
//         this.energy -= hour
//         console.log(`Welcome ${this.name}, enjoy the game`)
//     }
// }

// let basofi = new Character('Basofi',10);
// let sudirman = new Character('Sudirman',15);

// 4. Object.create()
//  Pros: good for performance
//  Cons: slobbery

// Logic Flow
// const methodCharacter = {
//     eat: function (portion) {
//         this.energy += portion;
//         console.log(`Welcome ${this.name}, bon appetite!`)
//     },

//     play: function (hour) {
//         this.energy -= hour
//         console.log(`Welcome ${this.name}, enjoy the game`)
//     }
// };

// function Character(name,energy) {
//     let character = {};
//     character.name = name;
//     character.energy = energy;
//     character.eat = methodCharacter.eat;    // manual init
//     character.play = methodCharacter.play;  // manual init

//     return character;
// }

// let basofi = Character('Basofi',10);
// let sudirman = Character('Sudirman',15);

// Implementation Object.create()
// const methodCharacter = {
//     eat: function (portion) {
//         this.energy += portion;
//         console.log(`Welcome ${this.name}, bon appetite!`)
//     },

//     play: function (hour) {
//         this.energy -= hour
//         console.log(`Welcome ${this.name}, enjoy the game`)
//     }
// };

// function Character(name,energy) {
//     let character = Object.create(methodCharacter);
//     character.name = name;
//     character.energy = energy;

//     return character;
// }

// let basofi = Character('Basofi',10);
// let sudirman = Character('Sudirman',15);

// 5. Prototype
//  Pros: most effective
//  Cons: tend to be difficult to understand

// Logic Flow
// function Character(name,energy) {
//     // let this = Object.create(Character.prototype);      // background
//     this.name = name;
//     this.energy = energy;

//     // return this;                                        // background
// }

// Character.prototype.eat = function (portion) {
//     this.energy += portion;
//     return `Welcome ${this.name}, bon appetite!`;
// }

// Character.prototype.play = function (hour) {
//     this.energy -= hour;
//     return `Welcome ${this.nama}, enjoy the game`;
// } 

// let basofi = new Character('Basofi',10);
// let sudirman = new Character('Sudirman',15);

// Implementation Class with Prototype
// class Character {
//     constructor (name,energy) {
//         this.name = name;
//         this.energy = energy;
//     };

//     eat (portion) {
//     this.energy += portion;
//     return `Welcome ${this.name}, bon appetite!`;
//     };

//     play (hour) {
//     this.energy -= hour;
//     return `Welcome ${this.nama}, enjoy the game`;
//     };
// }

// let basofi = new Character('Basofi',10);
// let sudirman = new Character('Sudirman',15);